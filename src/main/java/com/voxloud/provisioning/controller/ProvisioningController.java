package com.voxloud.provisioning.controller;

import com.voxloud.provisioning.consts.Consts;
import com.voxloud.provisioning.entity.DeviceEntity;
import com.voxloud.provisioning.exception.NotSupportedDeviceModelException;
import com.voxloud.provisioning.exception.NotSupportedOverrideFragmentException;
import com.voxloud.provisioning.service.ProvisioningService;
import com.voxloud.provisioning.utils.PrimitiveUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1")
public class ProvisioningController {

    private static final Logger logger = LoggerFactory.getLogger(ProvisioningController.class);

    @Autowired
    private ProvisioningService provisioningService;

    @GetMapping(value = "/provisioning/{mac}")
    public ResponseEntity<String> getConfiguration(@PathVariable String mac) {
        logger.info("[mac:" + PrimitiveUtils.getMd5(Consts.LOG_MAC_SALT + mac) + "]");
        if (!PrimitiveUtils.isValidMACAddress(mac)) {
            logger.info("Invalid MAC address [" + mac + "]");
            return new ResponseEntity("invalid_MAC_address", HttpStatus.BAD_REQUEST);
        }

        Optional<DeviceEntity> deviceDbOpt = provisioningService.findDevice(mac);
        if (!deviceDbOpt.isPresent()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        try {
            Pair<String, String> provisioningFileAndContentType = provisioningService.getProvisioningFile(deviceDbOpt.get());

            HttpHeaders headers = new HttpHeaders();
            headers.put("Content-Type", Collections.singletonList(provisioningFileAndContentType.getSecond()));

            return new ResponseEntity(provisioningFileAndContentType.getFirst(), headers, HttpStatus.OK);
        } catch (NotSupportedOverrideFragmentException e) {
            logger.error("[mac:" + PrimitiveUtils.getMd5(Consts.LOG_MAC_SALT + mac) + "]", e);
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (NotSupportedDeviceModelException e) {
            logger.error("[mac:" + PrimitiveUtils.getMd5(Consts.LOG_MAC_SALT + mac) + "]", e);
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}