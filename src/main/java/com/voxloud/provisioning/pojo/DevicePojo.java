package com.voxloud.provisioning.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DevicePojo {

    private String username;
    private String password;
    private Map<String, String> propConfig;
    private String overrideFragment;

}
