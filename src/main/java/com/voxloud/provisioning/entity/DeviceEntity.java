package com.voxloud.provisioning.entity;

import javax.persistence.*;

import com.voxloud.provisioning.pojo.DeviceModel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "DEVICE")
@Getter
@Setter
public class DeviceEntity {

    @Id
    @Column(name = "mac_address")
    private String macAddress;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private DeviceModel model;

    @Column(name = "override_fragment")
    private String overrideFragment;

    private String username;

    private String password;

}