package com.voxloud.provisioning.config;

import lombok.Data;
import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Configuration
@ConfigurationProperties
@Data
public class DeviceConfigurationProperties {

    private Map<String, String> provisioning;

}
