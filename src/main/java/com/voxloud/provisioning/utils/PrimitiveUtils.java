package com.voxloud.provisioning.utils;

import com.voxloud.provisioning.consts.Consts;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.regex.Pattern;

public class PrimitiveUtils {

    public static String getMd5(String value) {
        return DigestUtils.md5Hex(value);
    }

    public static boolean isValidMACAddress(String str) {
        if (str == null) {
            return false;
        }
        Pattern p = Pattern.compile(Consts.MAC_ADDRESS_REGEX);
        return p.matcher(str).matches();
    }

}
