package com.voxloud.provisioning.exception;

public class NotSupportedDeviceModelException extends Exception {
    public NotSupportedDeviceModelException() {
        super();
    }

    public NotSupportedDeviceModelException(String message) {
        super(message);
    }

    public NotSupportedDeviceModelException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotSupportedDeviceModelException(Throwable cause) {
        super(cause);
    }

    protected NotSupportedDeviceModelException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
