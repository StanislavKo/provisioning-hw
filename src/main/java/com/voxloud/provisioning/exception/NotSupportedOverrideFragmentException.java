package com.voxloud.provisioning.exception;

public class NotSupportedOverrideFragmentException extends Exception {
    public NotSupportedOverrideFragmentException() {
        super();
    }

    public NotSupportedOverrideFragmentException(String message) {
        super(message);
    }

    public NotSupportedOverrideFragmentException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotSupportedOverrideFragmentException(Throwable cause) {
        super(cause);
    }

    protected NotSupportedOverrideFragmentException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
