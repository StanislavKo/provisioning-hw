package com.voxloud.provisioning.service;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Table;
import javax.persistence.metamodel.Metamodel;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Profile("test")
public class DatabaseCleanupService implements InitializingBean {

    @Autowired
    private EntityManager entityManager;

    private List<String> tableNames;

    /**
     * Uses the JPA metamodel to find all managed types then try to get the [Table] annotation's from each (if present) to discover the table name.
     * If the [Table] annotation is not defined then we skip that entity (oops :p)
     */
    @Override
    public void afterPropertiesSet() {
        Metamodel metaModel = entityManager.getMetamodel();
        tableNames = metaModel.getManagedTypes().stream()
                .filter(t -> t.getJavaType().isAnnotationPresent(Table.class))
                .map(t -> t.getJavaType().getAnnotation(Table.class).name())
                .collect(Collectors.toList());
    }

    /**
     * Utility method that truncates all identified tables
     */
    @Transactional
    public void truncate() {
        entityManager.flush();
        entityManager.createNativeQuery("SET REFERENTIAL_INTEGRITY FALSE").executeUpdate();
        tableNames.stream().forEach(tableName -> entityManager.createNativeQuery("TRUNCATE TABLE " + tableName).executeUpdate());
        entityManager.createNativeQuery("SET REFERENTIAL_INTEGRITY TRUE").executeUpdate();
    }

}
