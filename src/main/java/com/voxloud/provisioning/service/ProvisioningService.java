package com.voxloud.provisioning.service;

import com.voxloud.provisioning.entity.DeviceEntity;
import com.voxloud.provisioning.exception.NotSupportedDeviceModelException;
import com.voxloud.provisioning.exception.NotSupportedOverrideFragmentException;
import org.springframework.data.util.Pair;

import java.util.Optional;

public interface ProvisioningService {

    Optional<DeviceEntity> findDevice(String macAddress);

    Pair<String, String> getProvisioningFile(DeviceEntity deviceDb) throws NotSupportedDeviceModelException, NotSupportedOverrideFragmentException;

}
