package com.voxloud.provisioning.service;

import com.voxloud.provisioning.config.DeviceConfigurationProperties;
import com.voxloud.provisioning.entity.DeviceEntity;
import com.voxloud.provisioning.exception.NotSupportedDeviceModelException;
import com.voxloud.provisioning.exception.NotSupportedOverrideFragmentException;
import com.voxloud.provisioning.pojo.DevicePojo;
import com.voxloud.provisioning.render.Renderer;
import com.voxloud.provisioning.render.RendererFactory;
import com.voxloud.provisioning.repository.DeviceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

@Service
public class ProvisioningServiceImpl implements ProvisioningService {

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private DeviceConfigurationProperties deviceConfigurationProperties;

    @Override
    public Optional<DeviceEntity> findDevice(String macAddress) {
        return deviceRepository.findById(macAddress);
    }

    public Pair<String, String> getProvisioningFile(DeviceEntity deviceDb) throws NotSupportedDeviceModelException, NotSupportedOverrideFragmentException {
        DevicePojo device = createDevice(deviceDb);

        Renderer renderer = RendererFactory.getRenderer(deviceDb.getModel());
        return Pair.of(renderer.render(device), renderer.getContentType());
    }

    private DevicePojo createDevice(DeviceEntity deviceDb) {
        String username = deviceDb.getUsername();
        String password = deviceDb.getPassword();
        Map<String, String> propConfig = deviceConfigurationProperties.getProvisioning();
        String overrideFragment = deviceDb.getOverrideFragment();
        return new DevicePojo(username, password, propConfig, overrideFragment);
    }

}
