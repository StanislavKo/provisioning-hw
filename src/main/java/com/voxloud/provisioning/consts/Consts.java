package com.voxloud.provisioning.consts;

public interface Consts {

    String LOG_MAC_SALT = "xS5M5iK2gNjgovVOMM";
    String MAC_ADDRESS_REGEX = "^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})|"
            + "([0-9a-fA-F]{4}\\.[0-9a-fA-F]{4}\\.[0-9a-fA-F]{4})$";

}
