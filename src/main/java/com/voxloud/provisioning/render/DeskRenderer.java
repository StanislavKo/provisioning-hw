package com.voxloud.provisioning.render;

import com.voxloud.provisioning.exception.NotSupportedOverrideFragmentException;
import com.voxloud.provisioning.pojo.DevicePojo;
import com.voxloud.provisioning.render.utils.RendererUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

public class DeskRenderer implements Renderer {

    private static final Logger logger = LoggerFactory.getLogger(DeskRenderer.class);

    @Override
    public String getContentType() {
        return "text/plain";
    }

    public String render(DevicePojo device) throws NotSupportedOverrideFragmentException {
        Properties properties = createDeviceData(device);
        parseApplicationProperties(properties, device);
        parseOverrideFragment(properties, device.getOverrideFragment());

        return properties.entrySet()
                .stream()
                .map(e -> e.getKey() + "=" + e.getValue())
                .collect(Collectors.joining("\n"));
    }

    private Properties createDeviceData(DevicePojo device) {
        Properties properties = new Properties();
        properties.put("username", device.getUsername());
        properties.put("password", device.getPassword());
        return properties;
    }

    private void parseApplicationProperties(Properties properties, DevicePojo device) {
        for (Map.Entry<String, String> entry : device.getPropConfig().entrySet()) {
            if ("codecs".equals(entry.getKey())) {
                properties.put(entry.getKey(), String.join(",", entry.getValue().split(",")));
            } else {
                properties.put(entry.getKey(), entry.getValue());
            }
        }
    }

    private void parseOverrideFragment(Properties properties, String overrideFragment) throws NotSupportedOverrideFragmentException {
        Map<String, Object> overrideFragmentMap = RendererUtils.parseOverrideFragment(overrideFragment);
        for (Map.Entry<String, Object> entry : overrideFragmentMap.entrySet()) {
            if (entry.getValue() instanceof List) {
                properties.put(entry.getKey(), String.join(",", (List) entry.getValue()));
            } else {
                properties.put(entry.getKey(), entry.getValue().toString());
            }
        }
    }

}
