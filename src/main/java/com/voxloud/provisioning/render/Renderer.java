package com.voxloud.provisioning.render;

import com.voxloud.provisioning.exception.NotSupportedOverrideFragmentException;
import com.voxloud.provisioning.pojo.DevicePojo;

public interface Renderer {

    String getContentType();

    String render(DevicePojo device) throws NotSupportedOverrideFragmentException;

}
