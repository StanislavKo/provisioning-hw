package com.voxloud.provisioning.render;

import com.voxloud.provisioning.exception.NotSupportedDeviceModelException;
import com.voxloud.provisioning.pojo.DeviceModel;

public class RendererFactory {

    public static Renderer getRenderer(DeviceModel model) throws NotSupportedDeviceModelException {
        if (model == null) {
            throw new NotSupportedDeviceModelException();
        }
        switch (model) {
            case DESK: return new DeskRenderer();
            case CONFERENCE: return new ConferenceRenderer();
            default: throw new NotSupportedDeviceModelException();
        }
    }

}
