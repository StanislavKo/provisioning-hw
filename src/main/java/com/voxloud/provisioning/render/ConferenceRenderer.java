package com.voxloud.provisioning.render;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.voxloud.provisioning.exception.NotSupportedOverrideFragmentException;
import com.voxloud.provisioning.pojo.DevicePojo;
import com.voxloud.provisioning.render.utils.RendererUtils;

import java.util.*;

public class ConferenceRenderer implements Renderer {

    @Override
    public String getContentType() {
        return "application/json";
    }

    @Override
    public String render(DevicePojo device) throws NotSupportedOverrideFragmentException {
        Map<String, Object> deviceData = createDeviceData(device);
        parseApplicationProperties(deviceData, device);
        Map<String, Object> overrideFragmentMap = RendererUtils.parseOverrideFragment(device.getOverrideFragment());
        deviceData.putAll(overrideFragmentMap);

        try {
            return new ObjectMapper().writeValueAsString(deviceData);
        } catch (JsonProcessingException e) {
            throw new NotSupportedOverrideFragmentException(e);
        }
    }

    private Map<String, Object> createDeviceData(DevicePojo device) {
        Map<String, Object> deviceData = new HashMap();
        deviceData.put("username", device.getUsername());
        deviceData.put("password", device.getPassword());
        return deviceData;
    }

    private void parseApplicationProperties(Map<String, Object> deviceData, DevicePojo device) {
        for (Map.Entry<String, String> entry : device.getPropConfig().entrySet()) {
            if ("codecs".equals(entry.getKey())) {
                deviceData.put(entry.getKey(), Arrays.asList(entry.getValue().split(",")));
            } else if (entry.getKey().contains(".")) {
                // processing properties like "provisioning.ap_prop_2.p21=v21" or "provisioning.ap_prop_3.p31.p311"
                List<String> keys = new ArrayList(Arrays.asList(entry.getKey().split("\\.")));
                Map<String, Object> keyMap = deviceData;
                for (int i = 0; i < keys.size(); i++) {
                    String key = keys.get(i);
                    if (!keyMap.containsKey(key)) {
                        keyMap.put(key, new HashMap<String, Object>());
                    }
                    if (i == keys.size() - 1) {
                        keyMap.put(key, entry.getValue());
                    } else {
                        keyMap = (Map<String, Object>) keyMap.get(key);
                    }
                }
            } else {
                deviceData.put(entry.getKey(), entry.getValue());
            }
        }
    }

}
