package com.voxloud.provisioning.render.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.voxloud.provisioning.exception.NotSupportedOverrideFragmentException;

import java.io.IOException;
import java.io.StringReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class RendererUtils {

    public static Map<String, Object> parseOverrideFragment(String overrideFragment) throws NotSupportedOverrideFragmentException {
        if (overrideFragment == null) {
            return Collections.EMPTY_MAP;
        }
        try {
            return new ObjectMapper().readValue(overrideFragment, HashMap.class);
        } catch (JsonProcessingException e) {
            if (!overrideFragment.contains("=")) {
                throw new NotSupportedOverrideFragmentException();
            }
            final Properties properties = new Properties();
            try {
                properties.load(new StringReader(overrideFragment));
            } catch (IOException ex) {
                throw new NotSupportedOverrideFragmentException();
            }
            Map<String, Object> result = new HashMap<>();
            for (Map.Entry<Object, Object> entry : properties.entrySet()) {
                result.put((String) entry.getKey(), entry.getValue());
            }
            return result;
        }
    }

}
