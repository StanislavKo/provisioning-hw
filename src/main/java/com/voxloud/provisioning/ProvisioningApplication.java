package com.voxloud.provisioning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.voxloud.provisioning.controller,com.voxloud.provisioning.entity,com.voxloud.provisioning.repository,com.voxloud.provisioning.service,com.voxloud.provisioning.config")
public class ProvisioningApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProvisioningApplication.class, args);
    }

}