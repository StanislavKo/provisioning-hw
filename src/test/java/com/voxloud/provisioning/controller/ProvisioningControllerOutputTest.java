package com.voxloud.provisioning.controller;

import com.voxloud.provisioning.service.DatabaseCleanupService;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@AutoConfigureMockMvc
public class ProvisioningControllerOutputTest {

    private static final Logger logger = LoggerFactory.getLogger(ProvisioningControllerOutputTest.class);

    @Autowired
    private DatabaseCleanupService databaseCleanupService;

    @Autowired
    private MockMvc mockMvc;

    @AfterAll
    public void afterAll() {
        logger.info("Cleanup up the test database");
        databaseCleanupService.truncate();
    }

    @Test
    public void shouldReturnBadRequest() throws Exception {
        this.mockMvc.perform(get("/api/v1/provisioning/asdf")).andDo(print()).andExpect(status().is(400));
    }

    @Test
    public void shouldReturnNotFound() throws Exception {
        this.mockMvc.perform(get("/api/v1/provisioning/01-23-45-67-89-AB")).andDo(print()).andExpect(status().is(404));
        this.mockMvc.perform(get("/api/v1/provisioning/01:23:45:67:89:AB")).andDo(print()).andExpect(status().is(404));
        this.mockMvc.perform(get("/api/v1/provisioning/0123.4567.89AB")).andDo(print()).andExpect(status().is(404));
    }

    @Test
    public void shouldReturnPropertiesFile() throws Exception {
        this.mockMvc.perform(get("/api/v1/provisioning/aa-aa-aa-aa-aa-01")).andDo(print()).andExpect(status().isOk()).andExpect(content().contentType("text/plain"));
    }

    @Test
    public void shouldReturnJsonFile() throws Exception {
        this.mockMvc.perform(get("/api/v1/provisioning/bb-bb-bb-bb-bb-01")).andDo(print()).andExpect(status().isOk()).andExpect(content().contentType("application/json"));
    }

}