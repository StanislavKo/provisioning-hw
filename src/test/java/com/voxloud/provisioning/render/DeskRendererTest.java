package com.voxloud.provisioning.render;

import com.voxloud.provisioning.exception.NotSupportedDeviceModelException;
import com.voxloud.provisioning.exception.NotSupportedOverrideFragmentException;
import com.voxloud.provisioning.pojo.DeviceModel;
import com.voxloud.provisioning.pojo.DevicePojo;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class DeskRendererTest {

    @Test
    public void testBasicConfig() throws NotSupportedDeviceModelException, NotSupportedOverrideFragmentException {
        Renderer renderer = RendererFactory.getRenderer(DeviceModel.DESK);
        Map<String, String> propConfig = new HashMap<>();
        propConfig.put("domain", "sip.voxloud.com");
        propConfig.put("port", "5060");
        propConfig.put("codecs", "G711,G729,OPUS");
        DevicePojo devicePojo = new DevicePojo("john", "doe", propConfig, null);

        Set<String> expectedResult = new HashSet<>();
        expectedResult.add("username=john");
        expectedResult.add("password=doe");
        expectedResult.add("domain=sip.voxloud.com");
        expectedResult.add("port=5060");
        expectedResult.add("codecs=G711,G729,OPUS");

        String actualResult = renderer.render(devicePojo);
        Set<String> actualResultSet = new HashSet(Arrays.asList(actualResult.split("\\n")));
        Assert.assertEquals(expectedResult, actualResultSet);
    }

    @Test
    public void testPropItems() throws NotSupportedDeviceModelException, NotSupportedOverrideFragmentException {
        Renderer renderer = RendererFactory.getRenderer(DeviceModel.DESK);
        Map<String, String> propConfig = new HashMap<>();
        propConfig.put("domain", "sip.voxloud.com");
        propConfig.put("port", "5060");
        propConfig.put("codecs", "G711,G729,OPUS");
        propConfig.put("ext1", "ext1v1");
        propConfig.put("ext2.ext21", "ext21v21");
        propConfig.put("ext2.ext22", "ext22v22");
        DevicePojo devicePojo = new DevicePojo("john", "doe", propConfig, null);

        Set<String> expectedResult = new HashSet<>();
        expectedResult.add("username=john");
        expectedResult.add("password=doe");
        expectedResult.add("domain=sip.voxloud.com");
        expectedResult.add("port=5060");
        expectedResult.add("codecs=G711,G729,OPUS");
        expectedResult.add("ext1=ext1v1");
        expectedResult.add("ext2.ext21=ext21v21");
        expectedResult.add("ext2.ext22=ext22v22");

        String actualResult = renderer.render(devicePojo);
        Set<String> actualResultSet = new HashSet(Arrays.asList(actualResult.split("\\n")));
        Assert.assertEquals(expectedResult, actualResultSet);
    }

    @Test
    public void testOverrideFragmentAsProperties() throws NotSupportedDeviceModelException, NotSupportedOverrideFragmentException {
        Renderer renderer = RendererFactory.getRenderer(DeviceModel.DESK);
        Map<String, String> propConfig = new HashMap<>();
        propConfig.put("domain", "sip.voxloud.com");
        propConfig.put("port", "5060");
        propConfig.put("codecs", "G711,G729,OPUS");
        DevicePojo devicePojo = new DevicePojo("john", "doe", propConfig, "domain=sip.anotherdomain.com\nport=5161\ntimeout=10\na1=v1,v2");

        Set<String> expectedResult = new HashSet<>();
        expectedResult.add("username=john");
        expectedResult.add("password=doe");
        expectedResult.add("domain=sip.anotherdomain.com");
        expectedResult.add("port=5161");
        expectedResult.add("codecs=G711,G729,OPUS");
        expectedResult.add("timeout=10");
        expectedResult.add("a1=v1,v2");

        String actualResult = renderer.render(devicePojo);
        Set<String> actualResultSet = new HashSet(Arrays.asList(actualResult.split("\\n")));
        Assert.assertEquals(expectedResult, actualResultSet);
    }

    @Test
    public void testOverrideFragmentAsJson() throws NotSupportedDeviceModelException, NotSupportedOverrideFragmentException {
        Renderer renderer = RendererFactory.getRenderer(DeviceModel.DESK);
        Map<String, String> propConfig = new HashMap<>();
        propConfig.put("domain", "sip.voxloud.com");
        propConfig.put("port", "5060");
        propConfig.put("codecs", "G711,G729,OPUS");
        DevicePojo devicePojo = new DevicePojo("john", "doe", propConfig, "{\"domain\":\"sip.anotherdomain.com\",\"port\":\"5161\",\"timeout\":10,\"a1\":[\"v1\", \"v2\"]}");

        Set<String> expectedResult = new HashSet<>();
        expectedResult.add("username=john");
        expectedResult.add("password=doe");
        expectedResult.add("domain=sip.anotherdomain.com");
        expectedResult.add("port=5161");
        expectedResult.add("codecs=G711,G729,OPUS");
        expectedResult.add("timeout=10");
        expectedResult.add("a1=v1,v2");

        String actualResult = renderer.render(devicePojo);
        Set<String> actualResultSet = new HashSet(Arrays.asList(actualResult.split("\\n")));
        Assert.assertEquals(expectedResult, actualResultSet);
    }

    @Test(expected = NotSupportedOverrideFragmentException.class)
    public void testOverrideFragmentIncorrect() throws NotSupportedDeviceModelException, NotSupportedOverrideFragmentException {
        Renderer renderer = RendererFactory.getRenderer(DeviceModel.DESK);
        Map<String, String> propConfig = new HashMap<>();
        propConfig.put("domain", "sip.voxloud.com");
        propConfig.put("port", "5060");
        propConfig.put("codecs", "G711,G729,OPUS");
        DevicePojo devicePojo = new DevicePojo("john", "doe", propConfig, "domain_sip.anotherdomain.com");

        renderer.render(devicePojo);
    }

}
