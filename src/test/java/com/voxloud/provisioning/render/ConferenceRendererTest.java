package com.voxloud.provisioning.render;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.voxloud.provisioning.exception.NotSupportedDeviceModelException;
import com.voxloud.provisioning.exception.NotSupportedOverrideFragmentException;
import com.voxloud.provisioning.pojo.DeviceModel;
import com.voxloud.provisioning.pojo.DevicePojo;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class ConferenceRendererTest {

    @Test
    public void testBasicConfig() throws NotSupportedDeviceModelException, NotSupportedOverrideFragmentException, JsonProcessingException {
        Renderer renderer = RendererFactory.getRenderer(DeviceModel.CONFERENCE);
        Map<String, String> propConfig = new HashMap<>();
        propConfig.put("domain", "sip.voxloud.com");
        propConfig.put("port", "5060");
        propConfig.put("codecs", "G711,G729,OPUS");
        DevicePojo devicePojo = new DevicePojo("john", "doe", propConfig, null);

        Map<String, Object> expectedResult = new HashMap<>();
        expectedResult.put("username", "john");
        expectedResult.put("password", "doe");
        expectedResult.put("domain", "sip.voxloud.com");
        expectedResult.put("port", "5060");
        expectedResult.put("codecs", Arrays.asList("G711,G729,OPUS".split(",")));

        String actualResult = renderer.render(devicePojo);
        Map<String, Object> actualResultSet = new ObjectMapper().readValue(actualResult, Map.class);
        Assert.assertEquals(expectedResult.entrySet(), actualResultSet.entrySet());
    }

    @Test
    public void testPropItems() throws NotSupportedDeviceModelException, NotSupportedOverrideFragmentException, JsonProcessingException {
        Renderer renderer = RendererFactory.getRenderer(DeviceModel.CONFERENCE);
        Map<String, String> propConfig = new HashMap<>();
        propConfig.put("domain", "sip.voxloud.com");
        propConfig.put("port", "5060");
        propConfig.put("codecs", "G711,G729,OPUS");
        propConfig.put("ext1", "ext1v1");
        propConfig.put("ext2.ext21", "ext21v21");
        propConfig.put("ext2.ext22", "ext22v22");
        DevicePojo devicePojo = new DevicePojo("john", "doe", propConfig, null);

        Map<String, Object> expectedResult = new HashMap<>();
        expectedResult.put("username", "john");
        expectedResult.put("password", "doe");
        expectedResult.put("domain", "sip.voxloud.com");
        expectedResult.put("port", "5060");
        expectedResult.put("codecs", Arrays.asList("G711,G729,OPUS".split(",")));
        expectedResult.put("ext1", "ext1v1");
        Map<String, String> ext2Map = new HashMap();
        ext2Map.put("ext21", "ext21v21");
        ext2Map.put("ext22", "ext22v22");
        expectedResult.put("ext2", ext2Map);

        String actualResult = renderer.render(devicePojo);
        Map<String, Object> actualResultSet = new ObjectMapper().readValue(actualResult, Map.class);
        Assert.assertEquals(expectedResult.entrySet(), actualResultSet.entrySet());
    }

    @Test
    public void testOverrideFragmentAsProperties() throws NotSupportedDeviceModelException, NotSupportedOverrideFragmentException, JsonProcessingException {
        Renderer renderer = RendererFactory.getRenderer(DeviceModel.CONFERENCE);
        Map<String, String> propConfig = new HashMap<>();
        propConfig.put("domain", "sip.voxloud.com");
        propConfig.put("port", "5060");
        propConfig.put("codecs", "G711,G729,OPUS");
        DevicePojo devicePojo = new DevicePojo("john", "doe", propConfig, "domain=sip.anotherdomain.com\nport=5161\ntimeout=10\na1=v1,v2");

        Map<String, Object> expectedResult = new HashMap<>();
        expectedResult.put("username", "john");
        expectedResult.put("password", "doe");
        expectedResult.put("domain", "sip.anotherdomain.com");
        expectedResult.put("port", "5161");
        expectedResult.put("codecs", Arrays.asList("G711,G729,OPUS".split(",")));
        expectedResult.put("timeout", "10");
        expectedResult.put("a1", "v1,v2");

        String actualResult = renderer.render(devicePojo);
        Map<String, Object> actualResultSet = new ObjectMapper().readValue(actualResult, Map.class);
        Assert.assertEquals(expectedResult.entrySet(), actualResultSet.entrySet());
    }

    @Test
    public void testOverrideFragmentAsJson() throws NotSupportedDeviceModelException, NotSupportedOverrideFragmentException, JsonProcessingException {
        Renderer renderer = RendererFactory.getRenderer(DeviceModel.CONFERENCE);
        Map<String, String> propConfig = new HashMap<>();
        propConfig.put("domain", "sip.voxloud.com");
        propConfig.put("port", "5060");
        propConfig.put("codecs", "G711,G729,OPUS");
        DevicePojo devicePojo = new DevicePojo("john", "doe", propConfig, "{\"domain\":\"sip.anotherdomain.com\",\"port\":\"5161\",\"timeout\":10,\"a1\":[\"v1\", \"v2\"]}");

        Map<String, Object> expectedResult = new HashMap<>();
        expectedResult.put("username", "john");
        expectedResult.put("password", "doe");
        expectedResult.put("domain", "sip.anotherdomain.com");
        expectedResult.put("port", "5161");
        expectedResult.put("codecs", Arrays.asList("G711,G729,OPUS".split(",")));
        expectedResult.put("timeout", 10);
        expectedResult.put("a1", Arrays.asList("v1,v2".split(",")));

        String actualResult = renderer.render(devicePojo);
        Map<String, Object> actualResultSet = new ObjectMapper().readValue(actualResult, Map.class);
        Assert.assertEquals(expectedResult.entrySet(), actualResultSet.entrySet());
    }

    @Test(expected = NotSupportedOverrideFragmentException.class)
    public void testOverrideFragmentIncorrect() throws NotSupportedDeviceModelException, NotSupportedOverrideFragmentException {
        Renderer renderer = RendererFactory.getRenderer(DeviceModel.CONFERENCE);
        Map<String, String> propConfig = new HashMap<>();
        propConfig.put("domain", "sip.voxloud.com");
        propConfig.put("port", "5060");
        propConfig.put("codecs", "G711,G729,OPUS");
        DevicePojo devicePojo = new DevicePojo("john", "doe", propConfig, "domain_sip.anotherdomain.com");

        renderer.render(devicePojo);
    }

}
