package com.voxloud.provisioning.render;

import com.voxloud.provisioning.exception.NotSupportedDeviceModelException;
import com.voxloud.provisioning.pojo.DeviceModel;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class RendererFactoryTest {

    @Test
    public void testGetSupported() throws NotSupportedDeviceModelException {
        assertNotNull(RendererFactory.getRenderer(DeviceModel.DESK));
        assertNotNull(RendererFactory.getRenderer(DeviceModel.CONFERENCE));
        for (DeviceModel model : DeviceModel.values()) {
            assertNotNull(RendererFactory.getRenderer(model));
        }
    }

    @Test(expected = NotSupportedDeviceModelException.class)
    public void testGetNotSupported() throws NotSupportedDeviceModelException {
        RendererFactory.getRenderer(null);
    }

}
