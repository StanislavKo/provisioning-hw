package com.voxloud.provisioning.utils;

import org.junit.Test;

import static org.junit.Assert.*;

public class PrimitiveUtilsTest {

    @Test
    public void testMd5() {
        assertEquals("912ec803b2ce49e4a541068d495ab570", PrimitiveUtils.getMd5("asdf"));
        assertEquals("5259ee4a034fdeddd1b65be92debe731", PrimitiveUtils.getMd5("912ec803b2ce49e4a541068d495ab570"));
    }

    @Test
    public void testMacAddressValidator() {
        assertTrue(PrimitiveUtils.isValidMACAddress("01-23-45-67-89-AB"));
        assertTrue(PrimitiveUtils.isValidMACAddress("01:23:45:67:89:AB"));
        assertTrue(PrimitiveUtils.isValidMACAddress("0123.4567.89AB"));
        assertFalse(PrimitiveUtils.isValidMACAddress("asdf"));
        assertFalse(PrimitiveUtils.isValidMACAddress("01-23-45-67-89-AH"));
        assertFalse(PrimitiveUtils.isValidMACAddress("01-23-45-67-AB"));
    }

}
