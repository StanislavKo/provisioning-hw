INSERT INTO device (mac_address, model, username, password, override_fragment) VALUES ('aa-aa-aa-aa-aa-01', 'DESK', 'john', 'doe', null);
INSERT INTO device (mac_address, model, username, password, override_fragment) VALUES ('bb-bb-bb-bb-bb-01', 'CONFERENCE', 'sofia', 'red', null);
INSERT INTO device (mac_address, model, username, password, override_fragment) VALUES ('aa-aa-aa-aa-aa-02', 'DESK', 'walter', 'white', STRINGDECODE('domain=sip.anotherdomain.com\nport=5161\ntimeout=10\na1=v1,v2'));
INSERT INTO device (mac_address, model, username, password, override_fragment) VALUES ('bb-bb-bb-bb-bb-02', 'CONFERENCE', 'eric', 'blue', '{"domain":"sip.anotherdomain.com","port":"5161","timeout":10,"a1":["v1", "v2"]}');
